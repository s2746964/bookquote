package nl.utwente.di.calculator;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Fahrenheit extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        String celsiusParam = request.getParameter("celsius");
        double celsius = Double.parseDouble(celsiusParam);
        double fahrenheit = 32 + 1.8 * celsius;

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>Celsius to Fahrenheit</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "  <P>Celsius: " + celsius + "\n" +
                "  <P>Fahrenheit: " + fahrenheit + "\n" +
                "</BODY></HTML>");
    }
}
